package com.gopi.expandablerecycler;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.content.Context.CONNECTIVITY_SERVICE;

public class PopularFragment extends Fragment {

    private String TAG = PopularFragment.class.getSimpleName();
    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private RequestQueue queue;
    private ArrayList<PhotoEntity> photoEntities = new ArrayList<>();
    private PeopleAdapter peopleAdapter;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_popular, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = view.findViewById(R.id.recyclerViewPhotoList);
        progressBar = view.findViewById(R.id.progressBar);
        progressBar.setMax(100);

        queue = Volley.newRequestQueue(getActivity());

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);

        peopleAdapter = new PeopleAdapter(getActivity(), photoEntities);

        if (checkNetworkConnection()) {
            progressBar.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);

            getPhotos();
        } else {
            progressBar.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            Toast.makeText(getActivity(), "No Internet Connection", Toast.LENGTH_SHORT).show();
        }

        recyclerView.setAdapter(peopleAdapter);
        peopleAdapter.notifyDataSetChanged();
    }

    private void getPhotos() {
        final String url = "https://jsonplaceholder.typicode.com/photos";
        photoEntities.clear();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "Response: " + response);
                        recyclerView.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);

                        try {
                            JSONArray array = new JSONArray(response);
                            for (int i = 0; i < array.length(); i++) {
                                JSONObject object = array.optJSONObject(i);
                                long albumId = object.optLong("albumId");
                                long id = object.optLong("id");
                                String title = object.optString("title");
                                String url = object.optString("url");
                                String thumbnailUrl = object.optString("thumbnailUrl");

                                PhotoEntity photoEntity = new PhotoEntity(albumId, id, title, url, thumbnailUrl);
                                photoEntities.add(photoEntity);
                            }

                            peopleAdapter.notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "Error: " + error.toString());
            }
        });

        queue.add(stringRequest);
    }

    private boolean checkNetworkConnection() {
        ConnectivityManager manager = (ConnectivityManager) getActivity().getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = manager.getActiveNetworkInfo();

        if (activeNetwork != null) {
            Log.v(TAG, "Network connection is turned ON");
            return true;
        } else {
            Log.v(TAG, "Network connection is turned OFF");
            return false;
        }
    }
}
