package com.gopi.expandablerecycler;

public class PhotoEntity {

    public long albumId;
    public long id;
    public String title;
    public String url;
    public String thumbnailUrl;

    public PhotoEntity(long albumId, long id, String title, String url, String thumbnailUrl) {
        this.albumId = albumId;
        this.id = id;
        this.title = title;
        this.url = url;
        this.thumbnailUrl = thumbnailUrl;
    }

    public long getAlbumId() {
        return albumId;
    }

    public long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getUrl() {
        return url;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }
}
