package com.gopi.expandablerecycler;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class PeopleAdapter extends RecyclerView.Adapter<PeopleAdapter.ViewHolder> {

    private ArrayList<PhotoEntity> photoEntities;
    private Context context;
    private ViewHolder oldViewHolder;

    public PeopleAdapter(Context context, ArrayList<PhotoEntity> photoEntities) {
        this.context = context;
        this.photoEntities = photoEntities;
    }

    @NonNull
    @Override
    public PeopleAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.people_view, viewGroup, false);
        return new PeopleAdapter.ViewHolder(itemView);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imgViewThumb;
        TextView txtViewTitle;
        ImageView imgViewUrl;
        boolean drop = true;

        ViewHolder(View view) {
            super(view);
            imgViewThumb = view.findViewById(R.id.imgViewThumb);
            txtViewTitle = view.findViewById(R.id.txtViewTitle);
            imgViewUrl = view.findViewById(R.id.imgViewUrl);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final PeopleAdapter.ViewHolder holder, int position) {
        PhotoEntity photoEntity = photoEntities.get(position);

        holder.txtViewTitle.setText(photoEntity.title);

        String thumb = photoEntity.thumbnailUrl;
        if (thumb != null && !thumb.isEmpty()) {
            Glide.with(context).load(thumb).into(holder.imgViewThumb);
        }

        String url = photoEntity.url;
        if (url != null && !url.isEmpty()) {
            Glide.with(context).load(url).into(holder.imgViewUrl);
        }

        holder.imgViewUrl.setVisibility(View.GONE);

        holder.imgViewThumb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (oldViewHolder != null) {
                    oldViewHolder.imgViewUrl.setVisibility(View.GONE);
                }

                if (holder.imgViewUrl.isShown()) {
                    holder.drop = false;
                }

                if (holder.drop) {
                    holder.imgViewUrl.setVisibility(View.VISIBLE);
                    holder.drop = false;
                    oldViewHolder = holder;
                } else {
                    holder.imgViewUrl.setVisibility(View.GONE);
                    holder.drop = true;
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return photoEntities.size();
    }
}
